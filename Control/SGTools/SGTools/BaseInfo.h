// This file's extension implies that it's C, but it's really -*- C++ -*-.

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: BaseInfo.h,v 1.11 2008-12-15 16:22:45 ssnyder Exp $

/**
 * @file  SGTools/BaseInfo.h
 * @author scott snyder
 * @date Nov 2005
 * @brief Provide an interface for finding inheritance information
 *        at run time.
 */

#ifndef SGTOOLS_BASEINFO_H
#define SGTOOLS_BASEINFO_H


#include "AthenaKernel/BaseInfo.h"


#endif // not SGTOOLS_BASEINFO_H

