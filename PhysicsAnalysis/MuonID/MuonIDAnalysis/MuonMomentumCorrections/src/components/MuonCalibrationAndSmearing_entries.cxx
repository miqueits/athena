#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"

#include "../TestMCASTTool.h"
#include "../CalibratedMuonsProvider.h"

DECLARE_COMPONENT( CP::MuonCalibrationAndSmearingTool )
DECLARE_COMPONENT( CP::TestMCASTTool )
DECLARE_COMPONENT( CP::CalibratedMuonsProvider )

